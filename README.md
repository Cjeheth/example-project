# Initial project setup

## API
- Restore / build the Web.Api project
- Run the Web.Api project (dotnet run)
- API documentation available at https://localhost:5001/swagger

## UI
- Restore packages in the Web.Ui directory
- Serve the Web.Ui project (ng serve)
- Frontend running at http://localhost:4200/
- API _must_ be running else requests will fail