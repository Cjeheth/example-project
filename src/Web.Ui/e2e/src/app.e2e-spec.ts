import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should render some content', () => {
    page.navigateTo();
    expect(page.getContent()).toBeTruthy();
  });
});
