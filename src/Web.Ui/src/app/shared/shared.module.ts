import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';

const commonModules = [CommonModule, HttpClientModule];
const materialModules = [MatButtonModule, MatCardModule, MatProgressSpinnerModule, MatTableModule, MatToolbarModule];

@NgModule({
  exports: [commonModules, materialModules]
})
export class SharedModule {}
