import { Component } from '@angular/core';
import { User, UsersService } from 'generated/api-services.generated';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  constructor(private usersService: UsersService) {}

  users$: Observable<User[]> = this.usersService.getUsers();
}
