import { NgModule } from '@angular/core';
import { API_BASE_URL, UsersService } from 'generated/api-services.generated';

import { environment } from '../../../environments/environment';
import { SharedModule } from '../../shared/shared.module';
import { NamePipe } from './name.pipe';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';

@NgModule({
  imports: [SharedModule, UsersRoutingModule],
  declarations: [UsersComponent, NamePipe],
  providers: [UsersService, { provide: API_BASE_URL, useValue: environment.apiBaseUrl }]
})
export class UsersModule {}
