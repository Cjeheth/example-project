import { async, TestBed } from '@angular/core/testing';
import { UsersService } from 'generated/api-services.generated';
import { of } from 'rxjs';

import { SharedModule } from '../../shared/shared.module';
import { NamePipe } from './name.pipe';
import { UsersComponent } from './users.component';

describe('UsersComponent', () => {
  let usersServiceStub: Partial<UsersService>;

  beforeEach(async(() => {
    usersServiceStub = {
      getUsers: () => of([])
    };

    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [UsersComponent, NamePipe],
      providers: [{ provide: UsersService, useValue: usersServiceStub }]
    }).compileComponents();
  }));

  it('should create the component', async(() => {
    const fixture = TestBed.createComponent(UsersComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));
});
