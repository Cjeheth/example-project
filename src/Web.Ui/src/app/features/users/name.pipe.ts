import { Pipe, PipeTransform } from '@angular/core';
import { User } from 'generated/api-services.generated';

@Pipe({
  name: 'name'
})
export class NamePipe implements PipeTransform {
  transform(user: User): string {
    if (!user) {
      return null;
    }
    return `${user.firstName} ${user.lastName}`.trim();
  }
}
