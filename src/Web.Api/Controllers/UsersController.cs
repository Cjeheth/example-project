﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using UserManagement;
using UserManagement.Models;

namespace Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserManager userManager;

        public UsersController(IUserManager userManager)
        {
            this.userManager = userManager;
        }

        [HttpGet]
        [SwaggerResponse(typeof(IEnumerable<User>))]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            var users = await userManager.GetUsers();
            return Ok(users);
        }

        [HttpGet("{id}")]
        [SwaggerResponse(typeof(User))]
        public async Task<ActionResult<User>> GetUser(Guid id)
        {
            var user = await userManager.GetUserById(id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        [HttpPost]
        [SwaggerResponse(typeof(User))]
        public async Task<ActionResult<User>> AddUser([FromBody] User userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var createdUser = await userManager.AddUser(userModel);
            return Ok(createdUser);
        }

        [HttpPut("{id}")]
        [SwaggerResponse(typeof(User))]
        public async Task<ActionResult<User>> UpdateUser(Guid id, [FromBody] User userModel)
        {
            if (!ModelState.IsValid || id != userModel.Id)
            {
                return BadRequest(ModelState);
            }

            var existingUser = await userManager.GetUserById(id);
            if (existingUser == null)
            {
                return NotFound();
            }

            var updatedUser = await userManager.UpdateUser(id, userModel);
            return Ok(updatedUser);
        }
        
        [HttpDelete("{id}")]
        [SwaggerResponse(HttpStatusCode.NoContent, typeof(void))]
        public async Task<ActionResult> DeleteUser(Guid id)
        {
            await userManager.RemoveUserById(id);
            return NoContent();
        }
    }
}
