﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using NJsonSchema;
using NSwag.AspNetCore;
using System.Reflection;

namespace Web.Api.Middleware
{
    public static class ApiConfiguration
    {
        public static IServiceCollection AddAndConfigureMvc(this IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            return services;
        }

        public static IApplicationBuilder UseAndConfigureApiDocs(this IApplicationBuilder app)
        {
            return app.UseSwaggerUi3(typeof(Startup).GetTypeInfo().Assembly, settings =>
            {
                settings.DocExpansion = "list";
                settings.GeneratorSettings.Title = "Example project";
                settings.GeneratorSettings.DefaultPropertyNameHandling = PropertyNameHandling.CamelCase;
            });
        }

        public static IApplicationBuilder UseAndConfigureCors(this IApplicationBuilder app)
        {
            return app.UseCors(config =>
            {
                config
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            });
        }
    }
}
