﻿using Bogus;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using UserManagement;
using UserManagement.Models;
using UserManagement.UserManager;

namespace Web.Api.Middleware
{
    public static class UserManagement
    {
        public static IServiceCollection AddAndConfigureUserManagementServices(this IServiceCollection services)
        {
            var numUsers = 300;
            var userGenerator = new Faker<User>()
                .RuleFor(u => u.Id, f => Guid.NewGuid())
                .RuleFor(u => u.FirstName, f => f.Name.FirstName())
                .RuleFor(u => u.LastName, f => f.Name.LastName())
                .RuleFor(u => u.Username, (f, u) => f.Internet.UserName(u.FirstName, u.LastName))
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FirstName, u.LastName));

            return services.AddSingleton<IUserManager, InMemoryUserManager>(x => new InMemoryUserManager(
                userGenerator.Generate(numUsers)
            ));
        }
    }
}
