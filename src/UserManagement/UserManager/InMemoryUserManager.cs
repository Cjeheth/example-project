﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Models;

namespace UserManagement.UserManager
{
    public class InMemoryUserManager : IUserManager
    {
        private readonly IList<User> users;

        public InMemoryUserManager() : this(new List<User>())
        {
        }

        public InMemoryUserManager(IList<User> users)
        {
            this.users = users;
        }

        public Task<IEnumerable<User>> GetUsers()
        {
            return Task.FromResult<IEnumerable<User>>(users);
        }

        public Task<User> GetUserById(Guid id)
        {
            var user = FindUserById(id);
            return Task.FromResult(user);
        }

        public Task<User> AddUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            var existingUser = FindUserById(user.Id);
            if (existingUser != null)
            {
                throw new InvalidOperationException("User already exists.");
            }

            if (user.Id == null)
            {
                user.Id = Guid.NewGuid();
            }

            users.Add(user);
            
            return Task.FromResult(user);
        }

        public Task<User> UpdateUser(Guid id, User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            var existingUser = FindUserById(user.Id);
            if (existingUser == null)
            {
                throw new InvalidOperationException("User does not exist.");
            }

            existingUser.Email = user.Email;
            existingUser.Username = user.Username;
            existingUser.FirstName = user.FirstName;
            existingUser.LastName = user.LastName;

            return Task.FromResult(existingUser);
        }

        private User FindUserById(Guid? id)
        {
            if (id == null)
            {
                return null;
            }

            return users.SingleOrDefault(x => x.Id == id.Value);
        }

        public async Task RemoveUserById(Guid id)
        {
            var existingUser = await GetUserById(id);
            if (existingUser != null)
            {
                users.Remove(existingUser);
            }
        }
    }
}
